import {http} from '@/api/request.js'

/**
 * 首页轮播图
 * 
 */
export const getIndexSwiper = (data) => http.post('/home/swiper', data)

/**
 * 首页商品分类
 * 
 */
export const getHomecategory = (data) => http.post('/home/category', data)

/**
 * 首页推荐商品
 * 
 */
export const getHomeGoods = (data) => http.post('/home/goods/list', data)