//const http = uni.$u.http
import {http} from '@/api/request.js'
/**
 * 注册
 * data ={
	 phonenumber: '182222222222',
	 password: '11'
	 
 }
 */
export const register = (data) => http.post('/register', data)

/**
 * 登录
 * data ={
	 phonenumber: '182222222222',
	 password: '11'
	 
 }
 */
export const login = (data) => http.post('/login', data)


