//const http = uni.$u.http
import {http} from '@/api/request.js'
/**
 * 获取分类
 * @param {*} data 
 */
export const getCategory = (data) => http.post('/category/list', data)