export const http = {
	post(url, data = {}) {
		return new Promise(function(resolve, reject) {
			// "Producing Code"（可能需要一些时间）
			uni.request({
				url: 'http://localhost:3000' + url, //
				data: data,
				method: 'POST',
				header: {
					'token': uni.getStorageSync('token') ,//自定义请求头信息
					'tenantid':'shop01'
				},
				
				success: (res) => {
					let data = res.data
					if (data.code == '200') {
						resolve(data.data)
					} else if (data.code == 401) {
						uni.navigateTo({
							url: '/pages/login/login',
							success() {
								uni.showToast({
									title: data.message,
									icon: "error"
								})
							}
						})
					} else if (data.code == '1') {
						uni.showToast({
							title: data.message||'请求出错了',
							icon: "error"
						})
					}
				},
				fail: (fail) => {
					reject(fail)
				}
			});
		});
	}
}
