import {
	http
} from '@/api/request.js'
/**
 * 获取购物车列表数据
 * @param {*} data 
 */
export const getCartList = (data) => http.post('/cart/list', data)

/**
 * 添加购物车
 * @param {*} data 
 * data={
	 goodsId :1, //商品id
	 num :1 //数量
 }
 */
export const addCart = (data) => http.post('/cart/add', data)


/**
 * 购物车设置数量
 * @param {*} data 
 * data={
	 cartId :1, //购物车id
	 num :1 //数量 结果
 }
 */
export const setCartCount = (data) => http.post('/cart/setCount', data)

///cart/setCount


/**
 * 删除购物车
 * @param {*} data 
 * data={
	 cartIds :[1,2] //购物车集合
 }
 */
export const deleteCarts = (data) => http.post('/cart/delete', data)
