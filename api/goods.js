import {
	http
} from '@/api/request.js'
/**
 * 商品详情
 * @param {*} data 
 * data={
	 id :1
 }
 */
export const getGoodsDetail = (data) => http.post('/home/goods/detail', data)