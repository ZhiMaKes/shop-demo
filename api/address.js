//const http = uni.$u.http
import {http} from '@/api/request.js'
/**
 * 添加地址
 * data={name:'地址1'}
 */
export const addAddress = (data) => http.post('/address/add', data)

/**
 * 添加地址列表
 * 
 */
export const getAddress = (data) => http.post('/address/list', data)
